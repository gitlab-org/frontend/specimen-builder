.PHONY: clean

all: _site

clean:
	rm -rf src/_data/fonts/ src/_data/fontdata.json src/css/fonts.css src/fonts/*.woff2 _site/

_site: node_modules
	yarn run fontdata
	yarn run build

node_modules:
	yarn install --frozen-lockfile
