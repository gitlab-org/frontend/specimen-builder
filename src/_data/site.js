const coverage = "Latin, Greek & Cyrillic";

module.exports = {
	title: "Specimen:", // Edit this with the name of your type specimen

	// This determines the reading direction of the text blocks and glyph table.
	// Options:
	// Left to right: "ltr"
	// Right to left: "rtl"
	// Left to right, top to bottom: "ltrttb"
	// Right to left, top to bottom: "rtlttb"
	direction: "ltr",

	description:
		"A design built on top of Specimen Skeleton, based on research insights into effective digital type specimens.", // The description of your type specimen.

	typeface: false, // The name of the typeface you are building
	link: false, // This is the URL used for the button
	about:
		"Font specimen for regression tests. More details <a href='https://gitlab-org.gitlab.io/frontend/fonts/'>about the fonts</a>", // All about the typeface
	designers: false, // And who designed it
	coverage, // This populates the link that anchors to the list of languages.
	language: coverage,

	// More info: https://css-tricks.com/essential-meta-tags-social-media/
	metatags: []
};
